from keras.models import Model, Sequential
from keras.layers import Dense, Dropout, Input, BatchNormalization
from keras.layers.core import Dense, Activation
from keras.layers import LeakyReLU
from keras import regularizers
from keras.constraints import max_norm

def BuildDNN(N_input, width, depth, activation , dropout, do_bn, number_of_classes, MN):
    model = Sequential()
    model.add(Dense(units=width, input_dim=N_input, kernel_constraint=max_norm(MN)))
    if do_bn:
        model.add(BatchNormalization(momentum=0.99, epsilon=0.001))
    if activation=='leakyrelu':
        model.add(LeakyReLU(alpha=0.1))
    else:
        model.add(Activation(activation))
    model.add(Dropout(dropout))

    for i in xrange(0, depth):
        model.add(Dense(width, kernel_constraint=max_norm(MN)))
        if do_bn:
            model.add(BatchNormalization(momentum=0.99, epsilon=0.001))
        if activation=='leakyrelu':
            model.add(LeakyReLU(alpha=0.1))
        else:
            model.add(Activation(activation))        
        # Dropout randomly sets a fraction of input units to 0 at each update during training time                                                                                                                 # which helps prevent overfitting.                                                                                                                                                                 
        model.add(Dropout(dropout))
    if number_of_classes>1:
        model.add(Dense(number_of_classes, activation='softmax'))
    else:
        model.add(Dense(1, activation='sigmoid'))
    return model

    
    
