import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from pandas.plotting import scatter_matrix

def plot_distribution(plot_folder, var , sample1, sample2, sample1_name, sample2_name):
    variable_to_plot=var
    #loop over all samples
    if(variable_to_plot.find('_btagw')!=-1):
        bins = np.linspace(0, 3, 50)
    elif(variable_to_plot.find('_pt')!=-1 and variable_to_plot.find('jet')!=-1):
        bins = np.linspace(25, 200, 50)
    elif(variable_to_plot.find('_phi')!=-1):
        bins = np.linspace(-3.2, 3.2, 25)
    elif(variable_to_plot.find('_Rapidity')!=-1):
        bins = np.linspace(-3, 3, 25)
    elif(variable_to_plot=='jet0_m'):
        bins = np.linspace(0, 120, 50)
    elif(variable_to_plot=='jet1_m'):
        bins = np.linspace(0, 100, 50)
    elif(variable_to_plot=='jet2_m'):
        bins = np.linspace(0, 50, 50)
    elif(variable_to_plot=='jet3_m'):
        bins = np.linspace(0, 30, 50)
    elif(variable_to_plot=='jet0_m'):
        bins = np.linspace(0, 120, 50)
    elif(variable_to_plot=='jet0_m'):
        bins = np.linspace(0, 120, 50)
    elif(variable_to_plot=='jet0_m'):
        bins = np.linspace(0, 120, 50)
    elif(variable_to_plot=='met'):
        bins = np.linspace(0, 300, 50)
    else:
        minmums=[min(sample1[var]), min(sample2[var])]
        maxmums=[max(sample1[var]), max(sample2[var])]
        bins = np.linspace(min(minmums), max(maxmums), 50)

    plot_name=plot_folder+'/%s.png' % variable_to_plot
    plt.hist(sample1[var], histtype='step', density=True, bins=bins, label='$'+sample1_name+'$', linewidth=2)
    plt.hist(sample2[var], histtype='step', density=True, bins=bins, label='$'+sample2_name+'$', linewidth=2)
    plt.xlabel(variable_to_plot)
    plt.ylabel('Abitrary Units')
    plt.legend(loc='best')
    plt.savefig(plot_name)
    plt.clf()
    return

def plot_correlations(pd_plot, varlist, plot_folder='plot/', plot_name='correlation'):
    #pd_plot: pandas data frame
    #varlist: list of variables for example ['jet0_pt', 'jet1_pt']
    plot_name=plot_folder+"/"+plot_name+".png"
    correlations = pd_plot[varlist].corr()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(correlations, vmin=-1, vmax=1, cmap=cm.coolwarm)
    fig.colorbar(cax)
    ticks = np.arange(0,len(varlist),1)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xticklabels(varlist)
    ax.set_yticklabels(varlist)
    ax.tick_params(top=False, bottom=True,labeltop=False, labelbottom=True)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    fig.tight_layout()
    plt.savefig(plot_name)
    plt.clf()
    return

def plot_scatter(pd_plot, varlist, plot_folder, plot_name, size=4):
    #size: int, in case there are too many variables in one scatter plot, one scatter plot is divided in small parts, the number of varibles in one figure=2*size 
    Input_cat=[]
    for i in range(0,len(varlist)-size+1, size):
        InputVariables_tmp=varlist[i:i+size]
        Input_cat.append(InputVariables_tmp)
    plot_name_tmp=plot_folder+"/"+plot_name

    for i in range(len(Input_cat)-1):
        for j in range(i+1, len(Input_cat)):
            Scatter_var=Input_cat[i]+Input_cat[j]
            scatter_matrix(pd_plot[Scatter_var], figsize=(18,18))
            plot_name_n=plot_name_tmp+'_%i%i.png' %(i,j)
            plt.savefig(plot_name_n)
            plt.clf()


if __name__=='_main_':
   print __name__
