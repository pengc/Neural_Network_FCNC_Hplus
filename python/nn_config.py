from prettytable import PrettyTable
import nn_model
from EarlyStop import EarlyStoppingbyOT
from keras import optimizers
from keras.callbacks import EarlyStopping, ModelCheckpoint

class NNConfig(object):
    
    def __init__(self, n_dim, n_depth, n_nodes, lr, batchsize, dropout, activation1, activation2, lossf, optimizer, epoch, patience,number_of_classes, MN):
        self.n_dim=n_dim
        self.n_depth=n_depth
        self.n_nodes=n_nodes
        self.lr=lr
        self.batchsize=batchsize
        self.dropout=dropout
        self.activation1=activation1
        self.activation2=activation2
        self.lossf=lossf
        self.optimizer=optimizer
        self.epoch=epoch
        self.patience=patience
        self.loss=100.
        self.accuracy=0.
        self.ROCI = 0.5
        self.num_classes=number_of_classes
        self.MN=MN

    def SetPerformance(self, loss, accuracy, ROCI):
        self.loss = loss
        self.accuracy=accuracy
        self.ROCI=ROCI

    def Show(self):
        print "==>NN Configration<=="
        NN_config=PrettyTable()
        NN_config.field_names= ["Options", "Value"]
        NN_config.add_row(["Number of input variables", self.n_dim])
        NN_config.add_row(["Number of hidder layer", self.n_depth])
        NN_config.add_row(["Number of nodes", self.n_nodes])
        NN_config.add_row(["Optimizer", self.optimizer])
        NN_config.add_row(["learning rate", self.lr])
        NN_config.add_row(["Batch size", self.batchsize])
        NN_config.add_row(["Dorpout", self.dropout])
        NN_config.add_row(["Max Norm Scale", self.MN])
        NN_config.add_row(["activation", self.activation1])
        NN_config.add_row(["activation for output", self.activation2])
        NN_config.add_row(["loss function", self.lossf])
        NN_config.add_row(["epoch", self.epoch])
        NN_config.add_row(["patience", self.patience])
        NN_config.add_row(["loss", self.loss])
        NN_config.add_row(["accuracy", self.accuracy])
        NN_config.add_row(["ROC Integral", self.ROCI])
        print NN_config
   
    def BuildDNNmodel(self):
        model = nn_model.BuildDNN(self.n_dim, self.n_nodes, self.n_depth, self.activation1, self.dropout, True, self.num_classes, self.MN)
        return model

    def SetOptimizer(self):
        if self.optimizer.upper()=='ADAM':
            optimizer_loc = optimizers.Adam(lr=self.lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0, amsgrad=False)
        else:
            optimizer_loc = optimizers.RMSprop(lr=self.lr, rho=0.9, epsilon=1e-08, decay=0.0)
        return optimizer_loc
    
    def SetCallBacks(self, OTCheck, modelname):
        callbacks = []
        callbacks.append(EarlyStopping(verbose=True, patience=self.patience, monitor='val_acc'))
        callbacks.append(ModelCheckpoint(modelname, monitor='val_acc', verbose=True, save_best_only=True, mode='max'))
        if OTCheck:
            callbacks.append(EarlyStoppingbyOT(monitor1='val_loss', monitor2='loss', patience=3 ,verbose=True))
        return callbacks
        

    
