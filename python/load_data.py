import pandas as pd
import sklearn.utils
from sklearn.preprocessing import StandardScaler,LabelEncoder
from sklearn.model_selection import train_test_split
import numpy as np
import os 

def GetSamples(sample, samplepath, label):
    pd_data=pd.DataFrame()
    
    print '====>Loading '+sample
    File=samplepath+"%s.pkl" % sample
    df_tmp = pd.read_pickle(File)
    print '    ----> Adding label %s' %label
    pd_data = Addlabel(df_tmp, label)
    return pd_data

def Randomizing(df):
    df = sklearn.utils.shuffle(df,random_state=123) #'123' is the random seed                                                                                                                              
    df = df.reset_index(drop=True) # drop=True does not allow the reset_index to insert a new column with the suffled index                                                                               
    return df


def Cut(df, cuts=['bjets_n=3', 'jets_n=4']):
    df_cuts=df
    for cut in cuts:
        if cut.find('=')>-1:
            cut_num=cut.split('=')
            if len(cut_num)==2:
                print "    ---->Cut on " + cut_num[0]+"=" + cut_num[1]
                df_cuts=df_cuts[df_cuts[cut_num[0]]==int(cut_num[1])]
            else:
                print "    ---->Worng cut formate, please check"

        elif cut.find('>')>-1:
            cut_num=cut.split('>')
            if len(cut_num)==2:
                print "    ---->Cut on " + cut_num[0]+">" + cut_num[1]
                df_cuts=df_cuts[df_cuts[cut_num[0]]>int(cut_num[1])]
            else:
                print "    ---->Worng cut formate, please check"

        elif cut.find('<')>-1:
            cut_num=cut.split('<')
            if len(cut_num)==2:
                print "    ---->Cut on " + cut_num[0]+"<" + cut_num[1]
                df_cuts=df_cuts[df_cuts[cut_num[0]]<int(cut_num[1])]
            else:
                print "    ---->Worng cut formate, please check"
        else:
            print 'Error: cut formate not recongnised'
   
    return df_cuts


def Addlabel(df, label):
    df['label'] = np.ones((df.shape[0]), dtype=int)*label
    return df


def CVSplit(df, no_split=2):
    # function that split a pandas dataframe into several part for CV, return value is a  list of pandas dataframe
    CV_samples=[]
    for i in range(0, no_split):
        CV_samples.append(df[i::no_split])

    return CV_samples

def load_data_CV(siglist, bkglist, filepath, class_n=4, n_fold=1, do_plots=False, cuts=["jets_n=4", "bjets_n=3"]):
    total_pd=[]
    #check siglist and bkg_list
    if len(siglist)==0 or len(bkglist)==0:
        print "Error:signal or background is empty, Please check"
        return total_pd
    # load data from .pkl file, two list of PD, PD_siglist=[[sig1_s1, sig1_s2...],[sig2_s1, sig2_s2...], ...] 
    PD_siglist=[]
    PD_bkglist=[]
    siglabellist =[]
    bkglabellist =[]
 
    if class_n==1:
        print "Doing binary classification"
        siglabellist = [1 for n in range(len(siglist))]
        bkglabellist = [0 for n in range(len(bkglist))]
    elif class_n==len(siglist)+len(bkglist):
        print "Doing multiclasses classification"
        siglabellist=[n for n in range(0,len(siglist))]
        bkglabellist=[n for n in range(len(siglist), len(bkglist)+len(siglist))]
    elif class_n!=len(siglist)+len(bkglist) and class_n==1+len(bkglist):
        print "Doing multiclasses classification"
        siglabellist = [0 for n in range(len(siglist))]
        bkglabellist = [n for n in range(1, len(bkglist)+1)]
    else:
        print "ERROR: incorrect number of classes!! please check"
            
    for si in range(0, len(siglist)):
        PD_sig_tmp=GetSamples(siglist[si], filepath, siglabellist[si])
        PD_sig_tmp=Cut(PD_sig_tmp, cuts)
        print '    ---->Total number of events: %s ' %(PD_sig_tmp.shape[0] )
        PD_sig_splited=CVSplit(PD_sig_tmp, n_fold)
        PD_siglist.append(PD_sig_splited)
        

    for bi in range(0, len(bkglist)):
        PD_bkg_tmp=GetSamples(bkglist[bi], filepath, bkglabellist[bi])
        PD_bkg_tmp=Cut(PD_bkg_tmp, cuts)
        print '    ---->Total number of events: %s ' %(PD_bkg_tmp.shape[0] )
        PD_bkg_splited=CVSplit(PD_bkg_tmp, n_fold)
        PD_bkglist.append(PD_bkg_splited)

    
    # merge PD with same split number (_sn) for both sig and bkg to form new PDlist PD=[sig_s1, sig_s2, ...]
       
    for n in range(0,n_fold):
        TotalPD_sig_tmp=pd.concat([s[n] for s in PD_siglist], ignore_index=True)
        TotalPD_bkg_tmp=pd.concat([b[n] for b in PD_bkglist], ignore_index=True)
        print "Total signal events in fold %i is: %i" %(n, TotalPD_sig_tmp.shape[0])
        print "Total background events in fold %i is: %i" %(n, TotalPD_bkg_tmp.shape[0])
        TotalPD_sig = Randomizing(TotalPD_sig_tmp)
        TotalPD_bkg = Randomizing(TotalPD_bkg_tmp)
        if TotalPD_bkg.shape[0]>TotalPD_sig.shape[0]:
            frames_2lj = [TotalPD_bkg[:TotalPD_sig.shape[0]],TotalPD_sig]
        else:
            frames_2lj = [TotalPD_bkg,TotalPD_sig[:TotalPD_bkg.shape[0]]]
        
        df_skim_2lj_tmp = pd.concat(frames_2lj,ignore_index=True)
        df_skim_2lj=Randomizing(df_skim_2lj_tmp) 
        total_pd.append(df_skim_2lj)
    print len(total_pd)
    return total_pd   

def load_data(siglist, bkglist, filepath, class_n, do_plots=False, cuts=["jets_n=4", "bjets_n=3"]):
    total_pd=[]
    #check siglist and bkg_list
    if len(siglist)==0 or len(bkglist)==0:
        print "Error:signal or background is empty, Please check"
        return total_pd
    # load data from .pkl file, two list of PD, PD_siglist=[[sig1_s1, sig1_s2...],[sig2_s1, sig2_s2...], ...] 
    PD_siglist=[]
    PD_bkglist=[]
    siglabellist =[]
    bkglabellist =[]
    if class_n==1:
        print "Doing binary classification"
        siglabellist = [1 for n in range(len(siglist))]
        bkglabellist = [0 for n in range(len(bkglist))]
    elif class_n==len(siglist)+len(bkglist):
        print "Doing multiclasses classification"
        siglabellist=[n for n in range(0,len(siglist))]
        bkglabellist=[n for n in range(len(siglist), len(bkglist)+len(siglist))]
    elif class_n!=len(siglist)+len(bkglist) and class_n==1+len(bkglist):
        print "Doing multiclasses classification"
        siglabellist = [0 for n in range(len(siglist))]
        bkglabellist = [n for n in range(1, len(bkglist)+1)]
    else:
        print "ERROR: incorrect number of classes!! please check"

    for si in range(0, len(siglist)):
        PD_sig_tmp=GetSamples(siglist[si], filepath, siglabellist[si])
        PD_sig_tmp=Cut(PD_sig_tmp, cuts)
        PD_sig_tmp=Randomizing(PD_sig_tmp)
        print '    ---->Total number of events: %s ' %(PD_sig_tmp.shape[0])
        PD_siglist.append(PD_sig_tmp)
        
    for bi in range(0, len(bkglist)):
        PD_bkg_tmp=GetSamples(bkglist[bi], filepath, bkglabellist[bi])
        PD_bkg_tmp=Cut(PD_bkg_tmp, cuts)
        print '    ---->Total number of events: %s ' %(PD_bkg_tmp.shape[0])
        PD_bkglist.append(PD_bkg_tmp)

    if class_n==1:
        print "====> For binary class classification, Select the number of signal event equal no. background event."
        Total_PD_bkg=pd.concat(PD_bkglist, ignore_index=True)
        Total_PD_sig=pd.concat(PD_siglist, ignore_index=True)

        Total_PD_sig = Randomizing(Total_PD_sig)
        Total_PD_bkg = Randomizing(Total_PD_bkg)

        if Total_PD_bkg.shape[0]>Total_PD_sig.shape[0]:
            frames_2lj = [Total_PD_bkg[:Total_PD_sig.shape[0]],Total_PD_sig]
        else:
            frames_2lj = [Total_PD_bkg,Total_PD_sig[:Total_PD_bkg.shape[0]] ]
        Total_PD_tmp=pd.concat(frames_2lj, ignore_index=True)
        Total_PD=Randomizing(Total_PD_tmp)
        total_pd.append(Total_PD)
        print "====>Total number of events after selection: %s " %(Total_PD.shape[0])
    else:
        print "====>For multi-class classification"
        total_pd_list=PD_siglist+PD_bkglist
        Total_PD_tmp=pd.concat(total_pd_list, ignore_index=True)
        Total_PD=Randomizing(Total_PD_tmp)
        total_pd.append(Total_PD)
        print "====>Total number of events after selection: %s " %(Total_PD.shape[0])
    return total_pd



def train_test_pre(total_pd, ith_fold, InputVariables, label):
    le = LabelEncoder()
    scaler = StandardScaler()
    if len(total_pd)==1:
        print "Prepareing training and testing data, Ignoring Cross Validation"
        X_pd = total_pd[0][InputVariables+['jets_n', 'bjets_n']]
        y_tmp = total_pd[0]['label'] 
        w_pd = total_pd[0]['nomWeight_weight_pu']
        y_pd = le.fit_transform(y_tmp)
        X_train_val, X_test, y_train_val, y_test, w_train_val, w_test = train_test_split(X_pd, y_pd, w_pd, train_size=0.7,random_state=123)
        X_train, X_val, y_train, y_val, w_train, w_val = train_test_split(X_train_val, y_train_val, w_train_val, train_size=0.7, random_state=123)
              
    else: 
        print "In preparing training and testing data for fold %s" %(int(ith_fold)+1)
        if len(total_pd)<3:
            print "Error, expected n_folder should no less than 3"
            
        X_train = total_pd[int(ith_fold)%(len(total_pd))][InputVariables+['jets_n', 'bjets_n']]
        X_val = total_pd[(int(ith_fold)+1)%(len(total_pd))][InputVariables+['jets_n', 'bjets_n']]
        X_test = total_pd[(int(ith_fold)+2)%(len(total_pd))][InputVariables+['jets_n', 'bjets_n']]

        y_train_tmp=total_pd[int(ith_fold)]['label'] 
        y_val_tmp=total_pd[(int(ith_fold)+1)%len(total_pd)]['label'] 
        y_test_tmp=total_pd[(int(ith_fold)+2)%len(total_pd)]['label'] 

        y_train=le.fit_transform(y_train_tmp)
        y_val=le.fit_transform(y_val_tmp)
        y_test=le.fit_transform(y_test_tmp)

    print "====>Total training sample size:" #, X_train.shape
    print "====>Total validation sample size:"#, X_val.shape
    print "====>Total testing sample size:", #X_test.shape
    return X_train, y_train, X_val, y_val, X_test, y_test



if __name__=='__main__':
   print __name__
