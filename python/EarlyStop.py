import keras

class EarlyStoppingbyOT(keras.callbacks.Callback):
   def __init__(self, monitor1='val_loss', monitor2='loss', patience=0, verbose=0):
       self.monitor1=monitor1
       self.monitor2=monitor2
       self.patience=patience
       self.stopped_epoch = 0
       self.wait = 0
       self.verbose=verbose

   def on_train_begin(self, logs=None):
       self.wait = 0
       self.stopped_epoch = 0
  
   def on_epoch_end(self, epoch, logs=None):
       current1 = logs.get(self.monitor1)
       current2 = logs.get(self.monitor2)
       if current1 is None:
           warnings.warn("Early stopping requires %s available!" % self.monitor1, RuntimeWarning)
       if current1 >= current2:
          if self.verbose > 0:
              print("Epoch %05d: early stopping by Overtrain check" % epoch)
          self.model.stop_training = True
